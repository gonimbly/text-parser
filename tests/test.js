

const env = require('dotenv').config()
const assert = require('assert')
const request = require('supertest')

describe('environment', () => {
    /*
    No environment yet
    it('should have a database url', () => {
        assert(process.env.DATABASE_URL);
    });
    */
});

describe('parser', () => {

    let server;

    beforeAll(async () => {
        server = require('../index').server;
    });

    afterAll(async () => {
        server.close();
    });

    afterEach(async () => {

    });

    it('parses a string', async () => {

        const testData = require('./test-data/basic.json');
        const response = await request(server)
            .post(`/api/parse`)
            .send(testData)
            .expect(200);
        
        assert(response.text);
        const data = JSON.parse(response.text);
        const expected = {
            parsed: {
                datatype: "string",
                data: "information",
                action: "pull",
                otherData: "template",
                technology: "diff engine"
            }
        };

        assert.deepEqual(data, expected);
    });

    it('parses a string with an untemplated change', async () => {

        const testData = require('./test-data/untemplated-change.json');
        const response = await request(server)
            .post(`/api/parse`)
            .send(testData)
        .expect(200);
        
        assert(response.text);
        const data = JSON.parse(response.text);
        const expected = {
            parsed: {
                datatype: "string",
                data: "information",
                action: "pull",
                otherData: "template",
                technology: "diff engine"
            }
        };

        assert.deepEqual(data, expected);
    });

    it('parses a list using', async () => {

        const testData = require('./test-data/list.json');
        const response = await request(server)
            .post(`/api/parse`)
            .send(testData)
            .expect(200);

        assert(response.text);
        const data = JSON.parse(response.text);
        const expected = {
            parsed: {
                things: 'lists',
                food: [
                    {
                        number: '1',
                        item: 'Pizza'
                    }, {
                        number: '2',
                        item: 'Tacos'
                    }, {
                        number: '3',
                        item: 'Nachos'
                    }, {
                        number: '4',
                        item: 'Nothing'
                    }
                ],
                trickything: 'And'
            }
        };

        assert.deepEqual(data, expected);
    });

    it('parses a string with no space between the placeholder and the next piece of text', async () => {

        const testData = require('./test-data/no-space-change.json');
        const response = await request(server)
            .post(`/api/parse`)
            .send(testData)
            .expect(200);

        assert(response.text);
        const data = JSON.parse(response.text);
        const expected = {
            parsed: {
                environment: "something"
            }
        };

        assert.deepEqual(data, expected);
    });

    it('parses a string with no new lines', async () => {

        const testData = require('./test-data/no-newlines.json');
        const response = await request(server)
            .post(`/api/parse`)
            .send(testData)
            .expect(200);

        assert(response.text);
        const data = JSON.parse(response.text);
        const expected = {
            parsed: {
                foo: "bar"
            }
        };

        assert.deepEqual(data, expected);
    });
});