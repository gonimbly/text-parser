
//imports
const express = require('express');
const bodyParser = require('body-parser');

//configure express
const app = express();
app.use(bodyParser.json());

//Parser routes
const parser = require('./controllers/parser');
app.post('/api/parse/', parser.parse);

//startup
const server = app.listen(process.env.PORT || 9000, () => {
    const port = server.address().port;
    console.log('Example app listening at port %s', port);
});

//global access
module.exports.server = server;