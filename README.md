# Text Parser

## Commands
- **Install:** `npm i`
- **Run Tests:** `npm test`
- **Start Local Server:** `npm start`

## Visual Studio Debugging
Run `Jest All` debug mode to run all tests using jest

## To Do
- Add list delimiter to header parameters
- Add template validation and error handling
- Add support for lists with repeating line item formats (a, b alternation for example)
- Move route out of `parser.js`
- Clean up `parser.js` (move functions around, make sure division of labor makes sense)
- Deploy to Heroku and connect to Zapier
- Add basic authentication
- Create a performance test
- Add unit test coverage for `extractValue`, `parseText`, `parseHeader`, `parsePlaceholder`, and `parseParams`
- Add documentation for setting up project to `README.md`
- Move parsing to async process
- Add warning if the number of removed elements is != to the number of placeholders
- Add support for siblings?