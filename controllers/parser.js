
const diff = require('diff');

const HEADER_REGEX = /^{{((?:__)?[a-zA-Z_\-0-9]+(?:__)?)(?:\?([a-zA-Z_\-0-9=]*))*}}/;
const PLACEHOLDER_REGEX = /{{((?:__)?[a-zA-Z_\-0-9]+(?:__)?)(?:\?(.*))*}}/;

function extractValue(diffs, startIndex) {

    let endIndex;
    for (let i = startIndex; i < diffs.length; i++) {

        const diff = diffs[i];
        if (diff.removed) {
            endIndex = i - 1;
            break;
        }
    }

    const subList = diffs.slice(startIndex, endIndex);

    let value = '';
    for (let i = 0; i < subList.length; i++) {
        const diff = subList[i];
        value += diff.value;
    }

    return {
        value: value.trim(),
        length: subList.length
    }
}

function parseText(text, regex) {

    const matches = text.match(regex);

    if (matches) {
        const key = matches[1];
        const params = parseParams(matches[2]);
        return {
            key,
            params
        };
    }

    return {};
}

function parseHeader(text) {
    return parseText(text, HEADER_REGEX);
}

function parsePlaceholder(placeholder) {
    return parseText(placeholder, PLACEHOLDER_REGEX);
}

function parseParams(paramsString) {

    if (!paramsString) {
        return;
    }

    const pairs = paramsString.split('&');
    return pairs.reduce((parameters, pair) => {

        const [key, value] = pair.split('=');
        parameters[key] = value;
        return parameters;
    }, {});
}

function parse(req, res) {

    try {
        const body = req.body;
        const { text, templates } = body;
        const root = templates.root;

        res.send({
            parsed: parseTemplate(templates, text)
        });
    }
    catch (error) {
        console.log(error.stack);
        res.status(500).send({
            message: error.message,
            stack: error.stack
        });
    }
}

function parseTemplate(templates, text, templateName = 'root') {

    text = unescape(text);
    let template = unescape(templates[templateName]);

    const header = parseHeader(template);
    if (!header) {
        throw new Error('A valid header not found at the top of the template');
    }

    template = template.replace(HEADER_REGEX, '');

    let chunks = [text];
    if (header.key === '__start__' && header.params && header.params.type === 'list') {
        chunks = text.split(`\r\n`);
        template = `${template.split(`\r\n`)[1]}{__lineEnd__}`;
    }
    
    const result = chunks.map(chunk => {
        return parseBody(templates, template, chunk);
    });

    if (result.length == 1) {
        return result[0];
    }

    return result;
}

function parseBody(templates, template, chunk) {

    const diffWords = diff.diffWordsWithSpace(template, chunk, {
        tokenizer: /(\s+|[()[\]{}'"_]|\b)/
    });

    const parsed = {};
    for (let i = 0; i < diffWords.length; i++) {

        const item = diffWords[i];
        if (item.removed) {

            const { key, params } = parsePlaceholder(item.value);
            if (key) {

                let { value, length } = extractValue(diffWords, i + 1);
                i += length;

                if (!key || key === '__end__' || key === '__start__' || key === '__lineEnd__') {
                    continue;
                }

                if (params && params.child) {
                    value = parseTemplate(templates, value, params.child);
                }

                parsed[key] = value;
            }
        }
    }

    return parsed;
}

module.exports.parse = parse;